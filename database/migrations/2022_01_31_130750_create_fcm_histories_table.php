<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFcmHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('fcm_histories')){
        Schema::create('fcm_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fcm_message_id')->nullable();
            $table->integer('member_id')->nullable();
            $table->integer('shop_user_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('title')->nullable();
            $table->string('message')->nullable();
            $table->string('image')->nullable();
            $table->string('domain_url')->nullable();
            $table->string('user_token', 250);
            $table->string('multicast_id', 100);
            $table->integer('status');
            $table->text('fcm_response');
            $table->timestamps();
            $table->string('object', 45)->nullable();
            $table->integer('objectId')->nullable();
            $table->integer('seen')->nullable()->default(0);
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('fcm_histories')){
        Schema::dropIfExists('fcm_histories');
        }
    }
}
