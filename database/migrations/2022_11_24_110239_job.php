<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->date('date')->nullable();
            $table->text('description')->nullable();
            $table->text('price_doc')->nullable();
            $table->text('job_doc')->nullable();
            $table->text('job_requirement_doc')->nullable();
            $table->text('pickup_doc')->nullable();
            $table->string('estimated_day')->nullable();
            $table->integer('price')->nullable();
            $table->string('address')->nullable();
            $table->string('square')->nullable();
            $table->string("longitude")->nullable();
            $table->string("latitude")->nullable();
            $table->integer('status')->comment('0:pending,1:active,2:cancel')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
