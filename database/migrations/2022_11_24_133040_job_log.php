<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_log', function (Blueprint $table) {
            $table->id();
            $table->integer('job_id');
            $table->string("member_id")->nullable();
            $table->dateTime("clock_in")->nullable();
            $table->dateTime("clock_out")->nullable();
            $table->string("longitude")->nullable();
            $table->string("latitude")->nullable();
            $table->text("e_signature")->nullable();
            $table->string('file')->nullable();
            $table->string('status')->comment('0:pending,1:complete,2:cancel')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
