<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('filePath');
            $table->string('extension')->nullable();
            $table->double('size')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('objectId')->nullable();
            $table->string('object')->nullable();
            $table->enum('type', ['IMAGE', 'PDF', 'FILE', 'ZIP'])->default('FILE');
            $table->boolean('coverImage')->default(true);
            $table->json('resolutions')->nullable();
            $table->integer('optimized')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
