<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complete_jobs', function (Blueprint $table) {
            $table->id();
            $table->integer('job_log_id')->nullable();
            $table->integer('member_id')->nullable();
            $table->string('nails')->nullable();
            $table->string('dg')->nullable();
            $table->string('staples')->nullable();
            $table->string('weed_fabric')->nullable();
            $table->string('glue')->nullable();
            $table->string('seam_type')->nullable();
            $table->string('option1')->nullable();
            $table->string('option2')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
