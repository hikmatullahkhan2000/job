<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFcmMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('fcm_messages')){
        Schema::create('fcm_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('object', ['EVENTS'])->default('EVENTS');
            $table->integer('objectId')->default(0);
            $table->string('title');
            $table->string('message', 500);
            $table->string('image');
            $table->string('domain_url')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('fcm_messages')){
        Schema::dropIfExists('fcm_messages');
        }
    }
}
