<?php

return  [
    'USERNOTFOUND' => 300,
    'INVALIDTOKEN' => 302,
    'TOKENEXPIRE' => 301,
    'TOKENMISSING' => 303,
    'SUCCESS' => 200,
    'REQUEST_VALIDATAION_ERROR' => 400,
    'SUCCESS_MSG' => "Executed Successfully",
    'FIELDS_ERRORS' => 201,
    'HTTP_NOT_FOUND' => 404,
    'AVAILABLE_ROOMS_CONSUMED' => 405,
    'ITEM_STOCK_UPDATED' => 406,
    'HTTP_NOT_FOUND_MSG' => "Page Not Found",
    'RESOURCE_NOT_FOUND' => 202,
    'DATA_NOT_FOUND_MSG' => "Data Not Found",
    'DATA_NOT_SAVE' => 203,
    'DATA_NOT_SAVE_MSG' => "Data occur error while saving",
    'SHOP_NOT_ALLOWED' => 304,
    'REQUEST_AGAIN' => 306,
];

