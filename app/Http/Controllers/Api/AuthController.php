<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\TwilioController;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use App\Models\Member;
use App\models\MemberFcmToken;
use App\Models\Params;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class AuthController extends BaseController
{

    public function loginUser(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError("Validation Error.", $validator->errors());
            }

            if (!Auth::attempt($request->only(['email', 'password']))) {
                return $this->sendResult("Email & Password does not match with our record.", 401, (object)[]);
            }

            $user = User::where('email', $request->email)->first();

            if($request->filled('fcm_token')){
                $user->fcm_token = $request->fcm_token;
                $user->save();
            }

            $token = $user->createToken("API TOKEN")->plainTextToken;
            return $this->sendResult('User Logged-In Successfully.', config('codes.SUCCESS'), ['user' => $user, 'token' => $token], $request);

        } catch (Exception $e) {
            return $this->sendResult($e->getMessage(), config('codes.REQUEST_VALIDATAION_ERROR'), $request);
        }
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => [
                'required', function ($attribute, $value, $fail) {
                    if (!Hash::check($value, Auth::user()->password)) {
                        $fail('Old Password didn\'t match.');
                    }
                },
            ],
            'new_password' => 'required|confirmed|min:6',
            'new_password_confirmation' => 'required',
        ]);

        $user = $request->user();

        if ($validator->fails()) {
            return $this->sendResult($validator->errors()->first(), config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors(), $request);
        }

        try {
            DB::beginTransaction();

            $user->password = Hash::make($request->new_password);
            $user->save();

            DB::commit();

            $token = $user->createToken("API TOKEN")->plainTextToken;
            return $this->sendResult('Password changed successfully.', config('codes.SUCCESS'), ['user' => $user, 'token' => $token], $request);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->sendResult($exception->getMessage(), config('codes.REQUEST_VALIDATAION_ERROR'), $request);
        }
    }

    public function setPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password_confirmation' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return $this->sendResult($validator->errors()->first(), config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors(), $request);
        }

        $user = $request->user();
        try {
            DB::beginTransaction();
            $hashed = Hash::make($request->password);
            $user->password = $hashed;
            if (!$user->save()) {
                return $this->sendResult('User', config('codes.DATA_NOT_SAVE'), ['errors' => ['error' => ['Incorrect data.']]], $request);
            }

            DB::commit();

            $token = $user->createToken("API TOKEN")->plainTextToken;
            return $this->sendResult('Password saved successfully.', config('codes.SUCCESS'), ['user' => $user, 'token' => $token], $request);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->sendResult($exception->getMessage(), config('codes.REQUEST_VALIDATAION_ERROR'), $request);
        }
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|exists:users',
        ]);

        if ($validator->fails()) {
            return $this->sendResult($validator->errors()->first(), config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors(), $request);
        }

        try {
            $user = User::where('phone', $request->phone)->first();
            $token = $user->createToken("API TOKEN")->plainTextToken;

            return $this->sendResult('Success', config('codes.SUCCESS'), ['token' => $token], $request);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->sendResult($exception->getMessage(), config('codes.REQUEST_VALIDATAION_ERROR'), $request);
        }
    }

    public function sendOTP(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'phone' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return $this->sendResult($validator->errors()->first(), config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors(), $request);
        // }
//        $signature = $request->get("signature", false);
//        $token = (string)(rand(111111, 999999));

//        if ($request->get('phone') == '03120000000') {
//            $token = '123456';
//        }
//        if ($signature) {
//            unset($request['signature']);
//            if($signature=='abc'){
//                $signature='';
//            }
//            $opt_message = "$signature This is an automated SMS from clickncollect.pk. Your verification PIN is $token. Please enter the PIN for instant verification. Thank you";
//        } else {
//            $opt_message = "This is an automated SMS from clickncollect.pk. Your verification PIN is $token. Please enter the PIN for instant verification. Thank you";
//        }
//                    $response = SmsHelper::sendMessage($request->get('phone'), $opt_message)
         $otp = (string)(rand(111111, 999999));
         $phone = $request->user()->phone;
         $opt_message = "This is an automated SMS from . Your verification PIN is $otp. Please enter the PIN for instant verification. Thank you";
         TwilioController::sendSms($phone, $opt_message);

        // self::updateMember(['phone' => $phone, 'otp' => $otp],$user);
        $request->user()->otp = 123456;
        $request->user()->save();
        return $this->sendResult('OTP sent successfully', config('codes.SUCCESS'), (object)[], $request);
    }

    public function updateMember($userArr, $user)
    {
        if (!empty($user)) {
            $user->update(['otp' => $userArr['otp']]);
        } else {
            $user = User::create([
                'phone' => $userArr['phone'],
                'otp' => $userArr['otp'],
            ]);
        }
        return $user->toArray();
    }

    public function verifyOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required|min:4|max:6',
            'fcm_token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendResult($validator->errors()->first(), config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors(), $request);
        }
        $user = $request->user();

        if ($user->otp == $request->get('otp')) {
            $mytime = Carbon::now(env('APP_TIMEZONE'));
            $user->fcm_token = $request->get('fcm_token');
            $user->otp_verified_at = $mytime->toDateTimeString();
            $user->save();
            $token = $user->createToken("API TOKEN")->plainTextToken;

            return $this->sendResult('OTP verified successfully', config('codes.SUCCESS'), ['user' => $user, 'token' => $token], $request);
        } else {
            return $this->sendResult('OTP not verified', config('codes.REQUEST_VALIDATAION_ERROR'), null, $request);
        }
    }

    public static function updateFcmToken(User $user, $token): bool
    {
        $userFcmToken = MemberFcmToken::where('member_id', $user->id)->first();
        if (!empty($userFcmToken)) {
            $userFcmToken->update(['fcm_token' => $token]);
        } else {
            MemberFcmToken::create([
                'member_id' => $user->id,
                'fcm_token' => $token,
                'status' => 'Y'
            ]);
        }
        return true;
    }

    public function splash(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_type' => 'required',
            'version' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendResult($validator->errors()->first(), config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors(), $request);
        }
        try {
            $message = '';
            $param = Params::where('key', $request->device_type)->first();
            $version_int = 0;
            if ($param && $param->value) {
                $db_version = $param->value;
                $user_version = $request->version;
                $db_version_arr = explode('.', $db_version);
                $user_version_arr = explode('.', $user_version);
                if (isset($db_version_arr[0]) && isset($user_version_arr[0]) && $db_version_arr[0] > $user_version_arr[0]) {
                    $version_int = 2;
                    $message = "we've upgraded  please update your app and enjoy added functionality";
                } elseif (isset($db_version_arr[1]) && isset($user_version_arr[1]) && $db_version_arr[1] > $user_version_arr[1]) {
                    $version_int = 1;
                    $message = "we've upgraded  please update your app and enjoy added functionality";
                } elseif (isset($db_version_arr[2]) && isset($user_version_arr[2]) && $db_version_arr[2] > $user_version_arr[2]) {
                    $version_int = 0;
                    $message = 'You have minor updates';
                } else {
                    $message = 'You have already on updated version';
                }
            }
            $versionContent = "";
            if ($version_int != 0)
                $versionContent = Params::where('key', "versionContent")->first()->value;

            $apiKey = env("google_api_key");

            $user = '';
            if(!empty($request->user_id)){
                $user = User::where('id', '=', $request->user_id)->first();
            }
            $screen = '/login';

            if (!empty($user)) {
                if ($user->type == User::company_user) {
                    if (empty($user->otp_verified_at)) {
                        $screen = '/otp_view';
                    } else if (empty($user->password)) {
                        $screen = '/set_pass';
                    } else if (empty($user->company_id)) {
                        $screen = '/create_company';
                    }else{
                        $screen = '/dashboard';
                    }
                }elseif ($user->type == User::nurse) {
                    if (empty($user->otp_verified_at)) {
                        $screen = '/otp_view';
                    } else if (empty($user->password)) {
                        $screen = '/set_pass';
                    }else{
                        $screen = '/dashboard';
                    }
                }
            }


            return $this->sendResult($message, config('codes.SUCCESS'), ['version_int' => $version_int, 'google_api_key' => $apiKey, 'versionContent' => $versionContent, 'route' => $screen], $request);
        } catch
        (\Exception $exception) {
            return $this->sendResult($exception->getMessage(), config('codes.REQUEST_VALIDATAION_ERROR'), ['version_int' => 0], $request);
        }
    }

    public function editProfile(Request $request)
    {
        try {
            $user = $request->user();
            if(isset($request->first_name)) $user->first_name = $request->first_name;
            if(isset($request->last_name)) $user->last_name = $request->last_name;
            if(isset($request->email)) $user->email = $request->email;
            if(isset($request->phone)) $user->phone = $request->phone;
            if($request->file('photo')) $user->photo = $user->modelFileUpload($request->file("photo"));
            $user->update();

            return $this->sendResult('Profile Updated Successfully', 200, compact('user'));
        } catch (\Exception $exception) {
            return $this->sendResult($exception->getMessage(), config('codes.REQUEST_VALIDATAION_ERROR'), $request);
        }

    }

    public function uploadDocument(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'document' => 'required|file'
        ]);

        if ($validator->fails()) {
            return $this->sendResult($validator->errors()->first(), config('codes.REQUEST_VALIDATAION_ERROR'), $validator->errors(), $request);
        }

        try {
            $user = $request->user();
            if($request->file('document')) $user->document = (new User)->modelFileUpload($request->file("document"));
            $user->save();
            return $this->sendResult('Document Uploaded Successfully', 200, compact('user'));
        } catch (\Exception $exception) {
            return $this->sendResult($exception->getMessage(), config('codes.REQUEST_VALIDATAION_ERROR'), $request);
        }

    }
    public function params(Request $request)
    {
        $isText = $request->isText ?? false;

        $param = Params::where('key', $request->key)->first();

        if (!$param)
            return $this->sendResult('Param not found.', config('codes.REQUEST_VALIDATAION_ERROR'), [], $request);

        if ($isText) {
            return $param->value;
        } else {
            return $this->sendResult($param->key, config('codes.SUCCESS'), ['text' => $param->value], $request);
        }

    }

}
