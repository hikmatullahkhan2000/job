<?php
/**
 * Created by PhpStorm.
 * User: warispopal
 * Date: 4/3/19
 * Time: 3:52 PM
 */

namespace App\TraitLibraries;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait ModelSearch
{

    public function scopeSearch($query, Request $request,$filters = [])
    {
        if(empty($filters))
            $filters = $this->searchableColumns;

        foreach ($request->all() as $column => $value):

            if ($value!=="0" && empty($value))
                continue;

                if (!empty($filters[$column])) {

                    if (!empty($filters[$column]['dataType'])) {
                        $key = (!empty($filters[$column]['join'])) ? $filters[$column]['join'] : $column;
                        switch ($filters[$column]['dataType']) {
                            case 'equal':
                                $query->where($key, $value);
                                break;
                            case 'like':
                                $query->where($key, 'LIKE', "%$value%");
                                break;
                            case 'daterange':
                                $query->whereBetween($key, $this->getDateRange($value));
                                break;
                            case 'dateTime':
                                $query->whereBetween($key, $this->getDateRangeTime($value));
                                break;
                            case 'time':
                                    $res_array=$this->getRangeTime($key,$value);
                                    $keys=array_keys($res_array);
                                    $values=array_values($res_array);
//                                    dd($keys);
//
                                $query->where($keys[0],'>', date("H:i", strtotime($values[0])));
                                $query->where($keys[1],'<', date("H:i", strtotime($values[1])));
                                break;
                            case 'in':
                                $query->whereIn($key, $value);
                                break;
                            case 'custom':
                                $query->where(DB::raw('('.$key.')'), $value);
                                break;
                        }

                    } else {
                        $query->where($column, $value);
                    }
                }

        endforeach;
        return $query;
    }
}
