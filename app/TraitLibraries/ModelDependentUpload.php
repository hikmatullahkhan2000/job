<?php


namespace App\TraitLibraries;

use App\helpers\CommonHelper;
use App\Models\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait ModelDependentUpload
{

    public function createDirectory($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        return $dir;
    }

    public function getFilePath()
    {
        $filePath = env('IMAGE_DIRECTORY') . "/" . $this->getTable();
        $this->createDirectory($filePath);
        return $filePath;
    }


    public function getTableFilePath()
    {
        $filePath = $this->getTable();
        $this->createDirectory(env('IMAGE_DIRECTORY') . "/" . $filePath);
        return $filePath;
    }


//    public function modelImageUpload($photo, $saveToFile = false, $patches = [])
//    {
//        $user = Auth::user();
//        $mainDir = $this->getFilePath();
//        $uniqueName = $this->getCustomeUniqueFileTitle($photo->getClientOriginalName());
//        $extension = $photo->getClientOriginalExtension();
//
//        $save_name = "$uniqueName.$extension";
//        $full_dir = $mainDir . "/" . $save_name;
//        $photo->move($mainDir, $save_name);
//        $optimizerChain = OptimizerChainFactory::create();
//
//        $optimizerChain->optimize($full_dir);
//
////        if (!empty($patches)):
////
////            $file->resolutions = json_encode($patches);
////            foreach ($patches as $width => $hieght)
////
////                if (!empty($width) && !empty($hieght)) {
////                    $newImage = $this->createDirectory($mainDir . "/" . "{$width}x{$hieght}/") . $save_name;
////
////                    $img = Image::make($photo->getRealPath());
////
////                    $img->resize($width, $hieght, function ($constraint) {
////                        $constraint->aspectRatio();
////                    })->save($newImage);
////
////                    ImageOptimizer::optimize($newImage);
////                }
////        endif;
//
//        if ($saveToFile) {
//            $file = new File();
//            $file->type = File::TYPE_IMAGE;
//            $file->filePath = $this->getTable();
//            $file->object = strtoupper($this->getTable());
//            $file->objectId = $this->id;
//            $file->user_id = $user->id;
//            $file->title = $save_name;
//            $file->extension = $extension;
//            $file->resolutions = json_encode($patches);
//            if (!$file->save())
//                throw new \Exception("An error occur while saving");
//
//            return $file;
//        }
//
//
//        return $save_name;
//    }
    public function modelImageUpload($photo, $saveToFile = false, $patches = [])
    {
        $user = Auth::user();
        $mainDir = $this->getTable();
        $uniqueName = $this->getCustomeUniqueFileTitle($photo->getClientOriginalName());
        $extension = $photo->getClientOriginalExtension();
        $save_name = "$uniqueName.$extension";
        $full_dir = $mainDir . "/" . $save_name;
//        $photo->move($mainDir, $save_name);
//        $optimizerChain = OptimizerChainFactory::create();
//
//        $optimizerChain->optimize($full_dir);

//        Storage::disk('s3')->put($full_dir, $photo);
        $path = $photo->storeAs($mainDir, $save_name, 's3');


//        $url=Storage::disk('s3')->url($full_dir);
//        $url=Storage::disk('s3')->url($path);
//        dd($url,$path);
//        if (!empty($patches)):
//
//            $file->resolutions = json_encode($patches);
//            foreach ($patches as $width => $hieght)
//
//                if (!empty($width) && !empty($hieght)) {
//                    $newImage = $this->createDirectory($mainDir . "/" . "{$width}x{$hieght}/") . $save_name;
//
//                    $img = Image::make($photo->getRealPath());
//
//                    $img->resize($width, $hieght, function ($constraint) {
//                        $constraint->aspectRatio();
//                    })->save($newImage);
//
//                    ImageOptimizer::optimize($newImage);
//                }
//        endif;
        if ($saveToFile) {
            $file = new File();
            $file->type = File::TYPE_IMAGE;
            $file->filePath = $this->getTable();
            $file->object = strtoupper($this->getTable());
            $file->objectId = $this->id;
            $file->user_id = $user->id;
            $file->title = $save_name;
            $file->extension = $extension;
            $file->resolutions = json_encode($patches);
            if (!$file->save())
                throw new \Exception("An error occur while saving");

            return $file;
        }


        return $save_name;
    }


    public function modelFileUpload($file, $saveToFile = false,$imageType =false)
    {

        Storage::disk('local')->makeDirectory($this->getTable());
        $mainDir = storage_path('app/public') . '/' . $this->getTable();
        $user = Auth::user();
        $uniqueName = $this->getCustomeUniqueFileTitle($file->getClientOriginalName());
        $extension = $file->getClientOriginalExtension();
        $save_name = "$uniqueName.$extension";
//        $full_dir = $mainDir . "/" . $save_name;

        $file->move($mainDir, $save_name);

        if ($saveToFile) {
            $fileModel = new File();
            $fileModel->type = $imageType;
            $fileModel->filePath = $this->getTable();
            $fileModel->object = strtoupper($this->getTable());
            $fileModel->objectId = (!empty($this->id)) ? $this->id : '';
            $fileModel->user_id = $user->id;
            $fileModel->title = $save_name;
            $fileModel->extension = $extension;
            if (!$fileModel->save())
                throw new \Exception("An error occur while saving");

            return $fileModel;
        }


        return $save_name;
    }

    public function modelMoveFile($imagePath, $saveToFile = false)
    {

        $photo = CommonHelper::pathToUploadedFile($imagePath);
        $user = Auth::user();

        $mainDir = $this->getTableFilePath();
        $uniqueName = $this->getCustomeUniqueFileTitle($photo->getClientOriginalName());
        $extension = $photo->getClientOriginalExtension();
        $save_name = "$uniqueName.$extension";
        $savedPath = "$mainDir/$save_name";
        $toSavedPath = "public/$mainDir";

//        $photo->storeAs($toSavedPath, $save_name);
//        $optimizerChain = OptimizerChainFactory::create();
//        $optimizerChain->optimize(env('IMAGE_DIRECTORY') . "/" . $savedPath);
//
//
        $path = $photo->storeAs($mainDir, $save_name, 's3');


        if ($saveToFile) {
            $file = new File();
            $file->type = File::TYPE_IMAGE;
            $file->filePath = $this->getTable();
            $file->object = strtoupper($this->getTable());
            $file->objectId = $this->id;
            $file->user_id = $user->id;
            $file->title = $save_name;
            $file->extension = $extension;
            if (!$file->save())
                throw new \Exception("An error occur while saving");
            return $file;
        }

        return $save_name;
    }

    public function urlMoveFile($imagePath, $saveToFile = false)
    {
        $photo = CommonHelper::urlToUploadedFile($imagePath);
        $user = Auth::user();

        $mainDir = $this->getTableFilePath();
        $uniqueName = $this->getCustomeUniqueFileTitle($photo->getClientOriginalName());
        $extension = $photo->getClientOriginalExtension();
        $save_name = "$uniqueName.$extension";
        $savedPath = "$mainDir/$save_name";
        $toSavedPath = "public/$mainDir";
        $path = $photo->storeAs($mainDir, $save_name, 's3');
//        $photo->storeAs($toSavedPath, $save_name);
//        $optimizerChain = OptimizerChainFactory::create();
//        $optimizerChain->optimize(env('IMAGE_DIRECTORY') . "/" . $savedPath);

        if ($saveToFile) {
            $file = new File();
            $file->type = File::TYPE_IMAGE;
            $file->filePath = $this->getTable();
            $file->object = strtoupper($this->getTable());
            $file->objectId = $this->id;
            $file->user_id = $user->id;
            $file->title = $save_name;
            $file->extension = $extension;
            if (!$file->save())
                throw new \Exception("An error occur while saving");
            return $file;
        }

        return $save_name;
    }

    /**
     * @param $base64File
     * @return mixed
     */
    public function getbase64UploadFile($base64File)
    {
        $img = explode(',', $base64File);
        $ini = substr($img[0], 11);
        $type = explode(';', $ini);
        $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64File));
        $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString() . '.' . $type[0];
        file_put_contents($tmpFilePath, $fileData);

        $tmpFile = new \Symfony\Component\HttpFoundation\File\File($tmpFilePath);
        $file = new UploadedFile(
            $tmpFile->getPathName(),
            $tmpFile->getFilename(),
            $tmpFile->getMimeType(),
            0,
            true // Mark it as test, since the file isn't from real HTTP POST.
        );
        return $file;
    }

    public function base64Upload($base64File, $isSave = false)
    {
        $img = explode(',', $base64File);

        $ini = substr($img[0], 11);
        $type = explode(';', $ini);
        $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64File));
        $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString() . '.' . $type[0];
        file_put_contents($tmpFilePath, $fileData);

        $tmpFile = new \Symfony\Component\HttpFoundation\File\File($tmpFilePath);
        $file = new UploadedFile(
            $tmpFile->getPathName(),
            $tmpFile->getFilename(),
            $tmpFile->getMimeType(),
            0,
            true // Mark it as test, since the file isn't from real HTTP POST.
        );

        return $this->modelFileUpload($file, $isSave);
    }

    /**
     * @param $str
     * @return string|string[]|null
     */
    protected function getCustomeUniqueFileTitle($str)
    {
        $str = md5($str . time());
        $str = strip_tags($str);
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = strtolower($str);
        $str = html_entity_decode($str, ENT_QUOTES, "utf-8");
        $str = htmlentities($str, ENT_QUOTES, "utf-8");
        $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
        $str = str_replace(' ', '-', $str);
        $str = rawurlencode($str);
        $str = str_replace('%', '-', $str);
        $str = str_replace('_', '-', $str);
        $str = preg_replace('/-+/', '-', $str);

        return $str;
    }

}
