<?php

namespace App\Models;

use App\TraitLibraries\ModelDependentUpload;
use App\TraitLibraries\ModelHelper;
use App\TraitLibraries\ModelSearch;
use Illuminate\Database\Eloquent\Model;

class FcmMessage extends Model
{
    use ModelSearch, ModelHelper, ModelDependentUpload;

    protected $fillable = [
        "title",
        "message",
        "image",
        "domain_url",
        "status",
        "object",
        "objectId",
    ];

    public $rule = array(
        'title' => 'required|min:3',
        'message' => 'required|min:3',
    );
    public $updateRule = array(
        "title" => "required",
        'message' => 'required|min:3',
    );

    /**
     * The search attributes that are mass assignable.
     *
     * @var array
     */


    public function scopeFcmHistory($query)
    {
        $query->leftJoin('fcm_histories', 'fcm_histories.fcm_message_id', '=', 'fcm_messages.id');
        $query->leftJoin('members', 'members.id', '=', 'fcm_histories.member_id');

        $query->select('fcm_messages.*',
            'fcm_messages.title',
            'fcm_messages.message',
            'fcm_messages.image',
            'members.name',
            'fcm_histories.member_id'
        );

        return $query;
    }
}
