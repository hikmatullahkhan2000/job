<?php

namespace App\Models;

use App\TraitLibraries\ModelHelper;
use App\TraitLibraries\ModelSearch;
use Illuminate\Database\Eloquent\Model;

class FcmHistory extends Model
{
    use ModelSearch, ModelHelper;

    protected $fillable = [
        "fcm_message_id",
        'member_id',
        'shop_user_id',
        "user_token",
        "multicast_id",
        "status",
        "fcm_response",
//        "customer_id",
        "user_id",
        "title",
        "message",
        "image",
        "domain_url",
        "object",
        "objectId"
    ];

    /**
     * The search attributes that are mass assignable.
     *
     * @var array
     */

    public $searchableColumns = array(

        'user_token' =>
            array(
                'dataType' => 'like',
                'join' => 'invoices.title'
            ),
        'created' =>
            array(
                'dataType' => 'daterange',
                'join' => 'invoices.created_at'
            ),
    );


    public function scopeFcmHistory($query)
    {
        $query->leftJoin('members', 'members.id', '=', 'fcm_histories.member_id');
        $query->leftJoin('users', 'user_id', '=', 'fcm_histories.user_id');

        $query->select('fcm_histories.*',
            'members.name as memberName',
            'users.name as userName'
        );

        $query->groupBy('fcm_histories.id');

        return $query;
    }
}
