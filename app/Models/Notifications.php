<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    use HasFactory;

    const UNREAD = 0;
    const READ = 1;

    const statuses = [
        self::UNREAD => 'unread',
        self::READ   => 'read',
    ];

    const CHAT = 0;
    const NOTIFICATION = 1;
    const ALERT = 2;

    const type = [
        self::CHAT => 'chat',
        self::NOTIFICATION => 'notification',
        self::ALERT => 'alert',
    ];

    protected $fillable = [
        'title',
        'body',
        'type',
        'status',
        'sender_id',
        'receiver_id',
        'url',
        'data',
    ];

    public function sender(){
        return $this->hasOne(User::class,'uid','sender_id');
    }
}
