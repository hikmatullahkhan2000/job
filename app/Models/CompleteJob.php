<?php

namespace App\Models;

use App\TraitLibraries\ModelDependentUpload;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\FileHelpers;

class CompleteJob extends Model
{
    use HasFactory,ModelDependentUpload;
    protected $table        = 'complete_jobs';
    public static $filepath = "complete_jobs";

    protected $fillable = [
        'job_log_id',
        'member_id',
        'nails',
        'dg',
        'staples',
        'weed_fabric',
        'glue',
        'seam_type',
        'option1',
        'option2',
        'comments',
        'photo_seam',
        'photo_turf',
    ];

    public function jobLog(){
        return $this->hasOne(JobLog::class,'id','job_log_id');
    }

    public function member(){
        return $this->hasOne(User::class,'id','member_id');
    }

}
