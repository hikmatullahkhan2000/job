<x-app-layout>
    <x-slot name="header">
        {{ __('Job Logs') }}
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Member</th>
                                    <th>Job</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($jobLogs as $jobLog)
                                    <tr>
                                        <td>{{$jobLog->id}}</td>
                                        <td>
                                            <?php
                                                $firstName  = $jobLog->member->first_name ?? '';
                                                $lastName  = $jobLog->member->last_name ?? '';
                                                echo $firstName.' '.$lastName;
                                            ?>
                                        </td>
                                        <td>{{$jobLog->job->title ?? ''}}</td>
                                        <td>{{\App\Models\JobLog::statuses[$jobLog->status]}}</td>
                                        <td class="text-nowrap" title="{{ $jobLog->created_at }}">{{ date("d M, Y", strtotime($jobLog->created_at)) }}</td>
                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-slot name="scripts">
        <script>
            $(function () {
                $("#usersTable").DataTable({
                    order: [[0, 'desc']],
                });
            });
        </script>
    </x-slot>
</x-app-layout>
