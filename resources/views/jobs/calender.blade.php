<x-app-layout>
    <x-slot name="header">
        {{ __('Manage Jobs') }}
        <a href="{{ route('jobs.create') }}" class="btn btn-primary pull-right button-left">
            <i class="menu-icon tf-icons bx bx-plus"></i>Add New
        </a>
    </x-slot>

    <style>
        .fc-title {
            color: white; /* Set the event title color to white */
        }
    </style>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="container">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-slot name="scripts">
        <script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/moment.min.js'></script>
        <script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery.min.js'></script>
        <script src="http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery-ui.custom.min.js"></script>
        <script src='http://fullcalendar.io/js/fullcalendar-2.1.1/fullcalendar.min.js'></script>
        <script>
            $(document).ready(function() {
                $('#calendar').fullCalendar({
                    events: <?php echo $jobs; ?>
                });
            });
        </script>
    </x-slot>
</x-app-layout>
