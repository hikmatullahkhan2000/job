<x-app-layout>
    <x-slot name="header">
        Job Assign To Member
    </x-slot>

    <div class="row">
        <!-- FormValidation -->
        <div class="col-2"></div>
        <div class="row">
            <!-- FormValidation -->
            <div class="col-2"></div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" class="form" enctype="multipart/form-data"
                              action="{{route('jobAssignStore', [$model->id])}}">

                            @csrf

                            <div class="col-12">
                                <h6 class="fw-semibold">
                                    Job Assign To Member
                                </h6>
                                <hr class="mt-0">
                            </div>
                            <div class="row">
                                <h3>{{$model->title ?? ''}}</h3>
                                @foreach($members as $member)
                                    <?php
                                        $checked = false;
                                        foreach ($jobLogs as $jobLog){
                                            if($jobLog['member_id'] == $member->id){
                                                $checked = true;
                                                break;
                                            }
                                        };
                                    ?>
                                    <div class="col-2" >
                                        <label class="form-label" for="users">{{$member->first_name.' '.$member->last_name}}</label>
                                        <input type="checkbox" name="users[]" {{($checked)?'checked':''}} value="{{$member->id??''}}">
                                    </div>
                                @endforeach
                                @if ($errors->has('users'))
                                    <div class="fv-plugins-message-container text-danger">
                                        <div data-field="formValidationBio"
                                             data-validator="stringLength" >{{ $errors->first('users') }}</div>
                                    </div>
                                @endif

                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
            <!-- /FormValidation -->
        </div>
        <!-- /FormValidation -->
    </div>

    <x-slot name="scripts">

    </x-slot>

</x-app-layout>

<script>
</script>


