<x-app-layout>
    <x-slot name="header">
        {{ __('Job Complete Logs') }}
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0" id="usersTable">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Job</th>
                                    <th>Member</th>
                                    <th>Nails</th>
                                    <th>DG</th>
                                    <th>Staples</th>
                                    <th>Weed Fabric</th>
                                    <th>Glue</th>
                                    <th>Seam Type</th>
                                    <th>Option1</th>
                                    <th>Option2</th>
                                    <th>Comment</th>
                                    <th>Photo Seam</th>
                                    <th>Photo Turf</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($completeJobs as $completeJob)
                                    <tr>
                                        <td>{{$completeJob->id}}</td>
                                        <td>{{$completeJob->jobLog->job->title ?? ''}}</td>
                                        <td>
                                            <?php
                                            $firstName  = $completeJob->member->first_name ?? '';
                                            $lastName   = $completeJob->member->last_name ?? '';
                                            echo $firstName.' '.$lastName;
                                            ?>
                                        </td>
                                        <td>{{$completeJob->nails}}</td>
                                        <td>{{$completeJob->dg}}</td>
                                        <td>{{$completeJob->staples}}</td>
                                        <td>{{$completeJob->weed_fabric}}</td>
                                        <td>{{$completeJob->glue}}</td>
                                        <td>{{$completeJob->seam_type}}</td>
                                        <td>{{$completeJob->option1}}</td>
                                        <td>{{$completeJob->option2}}</td>
                                        <td>{{$completeJob->comments}}</td>
                                        <td class="text-nowrap">
                                            @if (!empty($completeJob->photo_seam))
                                                <a class="btn btn-sm btn-outline-info" href="{{URL::to('/storage').'/'.$completeJob->photo_seam ?? '#'}}" download=""><i class="tf-icons bx bx-sm bx-cloud-download"></i> Download</a>
                                            @else
                                                <span class="text-muted">N/A</span>
                                            @endif
                                        </td>
                                        <td class="text-nowrap">
                                            @if (!empty($completeJob->photo_turf))
                                                <a class="btn btn-sm btn-outline-info" href="{{URL::to('/storage').'/'.$completeJob->photo_turf ?? '#'}}" download=""><i class="tf-icons bx bx-sm bx-cloud-download"></i> Download</a>
                                            @else
                                                <span class="text-muted">N/A</span>
                                            @endif
                                        </td>
                                        <td class="text-nowrap" title="{{ $completeJob->created_at }}">{{ date("d M, Y", strtotime($completeJob->created_at)) }}</td>
                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-slot name="scripts">
        <script>
            $(function () {
                $("#usersTable").DataTable({
                    order: [[0, 'desc']],
                });
            });
        </script>
    </x-slot>
</x-app-layout>
