
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Subber') }}</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="icon" type="image/x-icon" href="{{asset('front_assets/img/favicon.png')}}">

    <!-- Scripts -->
    {{--    @vite(['resources/css/app.css', 'resources/js/app.js'])--}}


    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet"
    />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{!! asset('assets/vendor/fonts/boxicons.css') !!}"/>

    <!-- Core CSS -->
    <link rel="stylesheet" href="{!! asset('assets/vendor/css/core.css') !!}" class="template-customizer-core-css"/>
    <link rel="stylesheet" href="{!! asset('assets/vendor/css/theme-default.css') !!}"
          class="template-customizer-theme-css"/>
    <link rel="stylesheet" href="{!! asset('assets/css/demo.css') !!}"/>

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{!! asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') !!}"/>

    {{--    <link rel="stylesheet" href="{!! asset('assets/vendor/libs/apex-charts/apex-charts.css') !!}"/>--}}

    <link href="{{ asset('assets/vendor/js/select2/css/select2.min.css') }}" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/dt/jqc-1.12.4/dt-1.12.1/b-2.2.3/sl-1.4.0/datatables.min.css"/>
{{--    <link src="{{ asset('assets/vendor/datatable/css/editor.dataTables.css') }}">--}}

<!-- Page CSS -->

    <!-- Helpers -->
    <script src="{!! asset('assets/vendor/js/helpers.js') !!}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{!! asset('assets/js/config.js') !!}"></script>
</head>
{{--<body class="font-sans antialiased">--}}
{{--<div class="min-h-screen bg-gray-100">--}}
{{--@include('layouts.navigation')--}}

<!-- Page Heading -->
{{--    @if (isset($header))--}}
{{--        <header class="bg-white shadow">--}}
{{--            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">--}}
{{--                {{ $header }}--}}
{{--            </div>--}}
{{--        </header>--}}
{{--    @endif--}}

{{--<!-- Page Content -->--}}
{{--    <main>--}}
{{--        {{ $slot }}--}}
{{--    </main>--}}
{{--</div>--}}

<body>
<!-- Layout wrapper -->
<div class="container">
    <div class="authentication-wrapper authentication-basic container-p-y">
        <div class="authentication-inner">
            <!-- Register -->
        {!! $slot !!}
        <!-- /Register -->
        </div>
    </div>
</div>

<!-- / Layout wrapper -->

<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="{!! asset('assets/vendor/libs/jquery/jquery.js')!!}"></script>
<script src="{!! asset('assets/vendor/libs/popper/popper.js')!!}"></script>
<script src="{!! asset('assets/vendor/js/bootstrap.js')!!}"></script>
<script src="{!! asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')!!}"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/dt/jqc-1.12.4/dt-1.12.1/b-2.2.3/sl-1.4.0/datatables.min.js"></script>
<script src="{{ asset('assets/vendor/js/select2/select2.min.js') }}"></script>

{{--<script src="{!! asset('assets/vendor/js/menu.js')!!}"></script>--}}

{{--<link rel="stylesheet" type="text/css" href="Editor-2.0.10/css/editor.dataTables.css">--}}


{{--<script src="{{ asset('assets/vendor/datatable/dataTables.editor.js') }}"></script>--}}
{{--<script type="text/javascript" src="Editor-2.0.10/js/dataTables.editor.js"></script>--}}


<!-- endbuild -->

<!-- Vendors JS -->
{{--<script src="{!! asset('assets/vendor/libs/apex-charts/apexcharts.js')!!}"></script>--}}


{{--<script src="{!! asset('assets/js/main.js')!!}"></script>--}}

<!-- Page JS -->
{{--<script src="{!! asset('assets/js/dashboards-analytics.js')!!}"></script>--}}

<!-- Place this tag in your head or just before your close body tag. -->
{{--<script async defer src=" https://buttons.github.io/buttons.js"></script>--}}
@if (isset($scripts))
    {{ $scripts }}
@endif

</body>
</html>



{{--<!DOCTYPE html>--}}
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}
{{--    <head>--}}
{{--        <meta charset="utf-8">--}}
{{--        <meta name="viewport" content="width=device-width, initial-scale=1">--}}
{{--        <meta name="csrf-token" content="{{ csrf_token() }}">--}}

{{--        <title>{{ config('app.name', 'Laravel') }}</title>--}}

{{--        <!-- Fonts -->--}}
{{--        <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">--}}

{{--        <!-- Scripts -->--}}
{{--        @vite(['resources/css/app.css', 'resources/js/app.js'])--}}
{{--    </head>--}}
{{--    <body>--}}
{{--        <div class="font-sans text-gray-900 antialiased">--}}
{{--            {{ $slot }}--}}
{{--        </div>--}}
{{--    </body>--}}
{{--</html>--}}
