<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Subber') }}</title>
    <link rel="icon" type="image/x-icon" href="{{asset('front_assets/img/favicon.png')}}">
    <script src="https://cdn.ckeditor.com/4.19.0/standard-all/ckeditor.js"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    {{--    @vite(['resources/css/app.css', 'resources/js/app.js'])--}}
{{--    <script src="https://cdn.ckeditor.com/ckeditor5/10.0.1/classic/ckeditor.js"></script>--}}


    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">
    {{--    />--}}

<!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{!! asset('assets/vendor/fonts/boxicons.css') !!}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

    <!-- Core CSS -->
    <link rel="stylesheet" href="{!! asset('assets/vendor/css/core.css') !!}" class="template-customizer-core-css"/>
    <link rel="stylesheet" href="{!! asset('assets/vendor/css/theme-default.css') !!}"
          class="template-customizer-theme-css"/>
    <link rel="stylesheet" href="{!! asset('assets/css/demo.css') !!}"/>
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{!! asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') !!}"/>

    {{--    <link rel="stylesheet" href="{!! asset('assets/vendor/libs/apex-charts/apex-charts.css') !!}"/>--}}

    <link href="{{ asset('assets/vendor/js/select2/css/select2.min.css') }}" rel="stylesheet"/>

    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jqc-1.12.4/dt-1.12.1/b-2.2.3/sl-1.4.0/datatables.min.css"/> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css"/>

    <!-- Helpers -->
    {{--    <script src="{!! asset('assets/vendor/js/helpers.js') !!}"></script>--}}

    {{--    <script src="{!! asset('assets/js/config.js') !!}"></script>--}}
{{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/5.10.0/main.min.css" rel="stylesheet">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.css" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>

<body>
<!-- Layout wrapper -->
<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">
        <!-- Menu -->

    @include('layouts.navigation')
    <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">
            <!-- Navbar -->
            @include('layouts._header')
        <!-- / Navbar -->
            <!-- Content wrapper -->
            <div class="content-wrapper">
                <!-- Content -->

                <div class="container-fluid flex-grow-1 container-p-y">

                    @if(session()->has('success'))
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-center">
                                <div class="col-md-12 p-2 m-2 my-auto justify-content-center">
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        {!! session()->get('success') !!}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-center">
                                <div class="col-md-12 p-2 m-2 my-auto justify-content-center">
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        {!! session()->get('error') !!}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    {{ $slot }}
                </div>
                <!-- / Content -->


                <div class="content-backdrop fade"></div>
            </div>
            <!-- Content wrapper -->
        </div>
        <!-- / Layout page -->
    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->

<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="{!! asset('assets/vendor/libs/jquery/jquery.js')!!}"></script>
<script src="{!! asset('assets/vendor/libs/popper/popper.js')!!}"></script>
<script src="{!! asset('assets/vendor/js/bootstrap.js')!!}"></script>
<script src="{!! asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')!!}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jqc-1.12.4/dt-1.12.1/b-2.2.3/sl-1.4.0/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('assets/vendor/js/select2/select2.min.js') }}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


@if (isset($scripts))
    {{ $scripts }}
@endif
<script>
    $('.delete-confirm-id').on('click', function () {
        var id = $(this).attr('data-id');
        console.log(id)
        if (id != undefined) {
            swal.fire({
                title       : 'Are you sure?',
                text        : "You won't be able to revert this!",
                type        : 'warning',
                showCancelButton    : true,
                confirmButtonColor  : '#0CC27E',
                cancelButtonColor   : '#FF586B',
                confirmButtonText   : 'Yes!',
                cancelButtonText    : "No, cancel"
            }).then(function (isConfirm) {
                if (isConfirm.value) {
                    document.getElementById('delete_form_' + id).submit();
                }
            }).catch(swal.noop);
        }
    });



    $(".listing_delete_icon").click(function(){
        var form_id = $(this).attr('form_id');
        $('#'+form_id).submit()
    });

    $(".ajax-view-modal").click(function (e) {

        // e.defaultPrevented;
        let id = $(this).attr("data-modal-id");
        let url = $(this).attr("data-url");
        let object = $("#" + id);

        if (object.length) {
            object.remove();
            object.modal("show");
            return;
        }
        $.ajax({
            url: url,
            type: "GET",
            dataType: "html",
            success: function (html) {
                $(html).appendTo('body');
            },
            error: function (request, status, error) {
                $(request.responseText).appendTo('body');
                $("#" + id).modal("show");

            }, complete: function () {
                $("#" + id).modal("show");
                $('#mySelect2').select2({
                    dropdownParent: $("#" + id),
                    width: '100%'

                });
            }
        });
    });

    $(function () {


        $('body').tooltip({
            selector: '[data-bs-toggle=tooltip]'
        });

        $('.table-responsive').on('show.bs.dropdown', function () {
            $('.table-responsive').css( "overflow", "inherit" );
        });

        $('.table-responsive').on('hide.bs.dropdown', function () {
            $('.table-responsive').css( "overflow", "auto" );
        })
    });


</script>


</body>
</html>
