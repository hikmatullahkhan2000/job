<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="my-4">
        <a href="/">
                <img class="img-fluid d-block mx-auto" src="{{asset("front_assets/img/site-identify/logo.png")}}" alt="" width="50%">
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <!-- Dashboard -->
        <li class="menu-item {{request()->is('/')  ? 'active' : ''}}">
            <a href="{{url('/dashboard')}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>

        <li class="menu-item {{request()->segment(1) == 'users'  ? 'active' : ''}}">
            <a href="{{route("users.index")}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-group"></i>
                <div data-i18n="create">Users</div>
            </a>
        </li>

        <li class="menu-item {{request()->segment(1) == 'jobs'  ? 'active' : ''}}">
            <a href="{{route("jobs.index")}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-briefcase"></i>
                <div data-i18n="create">Jobs</div>
            </a>
        </li>
        <li class="menu-item {{request()->segment(1) == 'job-logs'  ? 'active' : ''}}">
            <a href="{{route("jobLogs")}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-detail"></i>
                <div data-i18n="create">Review Response</div>
            </a>
        </li>
        <li class="menu-item {{request()->segment(1) == 'job-complete'  ? 'active' : ''}}">
            <a href="{{route("jobComplete")}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-detail"></i>
                <div data-i18n="create">Complete job Forms</div>
            </a>
        </li>
        <li class="menu-item {{request()->segment(1) == 'job-calender'  ? 'active' : ''}}">
            <a href="{{route("jobCalender")}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-detail"></i>
                <div data-i18n="create">Job Calender</div>
            </a>
        </li>
        <li class="menu-item {{request()->segment(1) == 'params'  ? 'active' : ''}}">
            <a href="{{route("params.index")}}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-slider"></i>
                <div data-i18n="create">Params</div>
            </a>
        </li>

        {{--        <li class="menu-item {{request()->segment(1) == 'job-logs'  ? 'active' : ''}}">--}}
{{--            <a href="{{route("jobLogs")}}" class="menu-link">--}}
{{--                <i class="menu-icon tf-icons bx bx-briefcase"></i>--}}
{{--                <div data-i18n="create">JobLogs</div>--}}
{{--            </a>--}}
{{--        </li>--}}


        <li class="menu-item {{request()->segment(1) == 'profile'  ? 'active' : ''}}">
            <a href="{{route("profile.edit")}}?shift=schedule" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user"></i>
                <div data-i18n="create">Profile</div>
            </a>
        </li>
{{--        @if (Auth::user()->type == \App\Models\User::admin_user)--}}
{{--            <li class="menu-item {{request()->segment(1) == 'params'  ? 'active' : ''}}">--}}
{{--                <a href="{{route("params.index")}}" class="menu-link">--}}
{{--                    <i class="menu-icon tf-icons bx bx-slider"></i>--}}
{{--                    <div data-i18n="create">Params</div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}

{{--        @if (Auth::user()->type == \App\Models\User::company_user)--}}
{{--            <li class="menu-item {{request()->segment(1) == 'messages'  ? 'active' : ''}}">--}}
{{--                <a href="{{route("realTimeChat")}}" class="menu-link">--}}
{{--                    <i class="menu-icon tf-me bx-message"></i>--}}
{{--                    <div data-i18n="create">Messages</div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}

    </ul>
</aside>
