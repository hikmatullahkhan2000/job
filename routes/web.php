<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\{
    ProfileController,
    CompanyController,
    ParamController,
    UserController,
    JobController
};
use App\Models\Job;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function ()
{
    Route::get('/dashboard',function (){
        return view('dashboard');
    })->middleware(['verified']);

    Route::get('/', function () {
        return view('dashboard');
    })->middleware(['verified'])->name('dashboard');

    Route::middleware(['admin'])->group(function ()
    {
        Route::resources([
            'params' => ParamController::class,
        ]);
    });


    Route::resources([
        'users' => UserController::class,
        'jobs' => JobController::class,
    ]);

    Route::get('params/status/{id}', [ParamController::class, "toggleStatus"])->name('params.toggle-status');

    Route::get('job-calender', [JobController::class, 'jobCalender'])->name('jobCalender');
    Route::get('job-logs', [JobController::class, 'jobLogs'])->name('jobLogs');
    Route::get('job-assign-member/{id}', [JobController::class, 'jobAssignMember'])->name('jobAssignMember');
    Route::post('job-assignStore/{id}', [JobController::class, 'jobAssignStore'])->name('jobAssignStore');
    Route::delete('jobs/images/{id}', [JobController::class, 'imageDelete'])->name('jobs.images');
    Route::post('airtable-store', [JobController::class, "airtableStore"])->name('airtableStore');
    Route::get('job-complete', [JobController::class, 'jobComplete'])->name('jobComplete');


    Route::controller(ProfileController::class)->prefix('profile')->name('profile.')->group(function () {
        Route::get('/', 'edit')->name('edit');
        Route::patch('/', 'update')->name('update');
        Route::delete('/', 'destroy')->name('destroy');

        Route::view('/change-password', 'profile.change-password')->name('password');
        Route::post('/change-password', 'changePassword')->name('change-password');
    });
});

require __DIR__.'/auth.php';
